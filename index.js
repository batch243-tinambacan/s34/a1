

const express = require('express'); 			
const app = express(); 						
const port = 3000; 								
app.use(express.json()); 						
app.use(express.urlencoded({enteded:true}));	

// this route expects to receive a GET request at the base of URI "/home"
	app.get("/home", (req, res) => {					
		res.send("Welcome to the home page")						
	});


// create an array to use as our mock db
	let users = [];									
		app.post("/users", (req,res) => {

			if(req.body.username!=="" && req.body.password!=="") {
				users.push(req.body)
				
				res.send(users)
			}
		})

// this route expects to receive a GET request at the base of URI "/users"
	app.get("/users", (req, res) => {					
		res.send(users)						
	});


// this route expects to receive a DELETE request at the base of URI "/delete-user"

	app.delete('/delete-user', (req, res) => {
	let deleteUser = users.findIndex(item => item.id === req.query.id);
	users.splice(deleteUser, 1);
	res.send(`User ${req.body.username} has been deleted`) 
	});

app.listen(port, () => console.log(`Server running at port ${port}`));